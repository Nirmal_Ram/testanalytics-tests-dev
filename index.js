"use strict";
const dbjs = require("./db.js");
exports.handler = async (event) => {
  const db = await dbjs.get();

  const dbresponse = await db
    .collection("hotstar-testAnalytics")
    .find()
    .project({ uuid: 1, info: 1 })
    .toArray();
  let result = dbresponse.map((data) => {
    return {
      version: "1.2",
      uuid: {
        _id: data.uuid._id,
        testId: data.uuid.testId,
        sessionId: data.uuid.sessionId,
        userDisplayName: data.uuid.userDisplayName,
        userEmail: data.uuid.userEmail,
        username: data.uuid.username,
      },
      testStatus: data.info.testStatus,
      testStartTime: data.info.testStartTime,
      projectName: data.info.projectName,
      appVersion: data.info.appVersion,
      scriptName: data.info.scriptName,
      deviceName: data.info.deviceName,
    };
  });

  const response = {
    statusCode: 200,
    body: result,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};
